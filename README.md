NEMASYSTEM - 2020

O objetivo deste projeto é automatizar a entrada, processamento e saída dos dados de análise nematológicos. 

<p>  
      <center>ㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤ</center>
  <img src="./imgs/logo.jpeg" title="Nemasystem Logo">
</p>

<h1 align="center">1.0 ENTREGA 01 (05/03/2020)</h1>
<ul> 
<li><b>1.1</b> [MAPA DO CONHECIMENTO]( )</li>
</ul>

<h1 align="center">2.0 ENTREGA 02 (10/03/2020)</h1>
<ul>
<li><b>2.1</b> [MVP - PRODUTO MÍNIMO VIÁVEL]( )</li>
<li><b>2.2</b> [CRONOGRAMA]( )</li>
</ul>

<h1 align="center">3.0 ENTREGA 03 (26/03/2020)</h1>
<ul>
<li><b>3.1</b> [CRONOGRAMA]( )</li>
<li><b>3.2</b> [MAPA DO CONHECIMENTO]( )</li>
<li><b>3.3</b> [MATRIZ DE HABILIDADES INICIAL]( )</li>
<li><b>3.4</b> [PROJEC CHARTER]( )</li>
</ul>

<h1 align="center">4.0 ENTREGA 04 (31/03/2020)</h1>
<ul>
<li><b>4.1</b> [MONTAGEM DO AMBIENTE DE DESENVOLVIMENTO]( )</li>
<li><b>4.2</b> [MONTAGEM DO GITLAB]( )</li>
<li><b>4.3</b> [EQUIPE E CURRICULOS]( )</li>
<li><b>4.4</b> [MAPA DE DEFINIÇÃO TECNOLÓGICA]( )</li>
</ul>

<h1 align="center">5.0 SPRINT 01 (16/04/2020)</h1>
<ul>
<li><b>5.1</b> [ARQUITETURA DE SOLUÇÃO]( )</li>
<li><b>5.2</b> [DIAGRAMA DE TELAS (ORGANOGRAMA)]( )</li>
</ul>

<h1 align="center">6.0 SPRINT 02 (30/04/2020)</h1>
<ul>
<li><b>6.1</b> [MODELAGEM DE DADOS]( )</li>
<li><b>6.2</b> [MOCK UP DAS TELAS (DESENHO DAS TELAS]( )</li>
<li><b>6.3</b> [CRUD DE USUARIO]( )</li>
<li><b>6.4</b> [RECUPERAÇÃO DE SENHA POR EMAIL]( )</li>
<li><b>6.5</b> [IMPLEMENTAÇÃO DA FUNCIONALIDADE DO ALGORITMO DE ANÁLISE NEMATOLÓGICA] ( )</li>
<li><b>6.6</b> [IMPLANTAÇÃO DO BANCO DE DADOS NO SERVIDOR]( )</li>
</ul>       

<h1 align="center">7.0 SPRINT 03 (30/04/2020)</h1>
<ul>
<li><b>4.3.1</b> [CONFIGURAÇÃO DO BANCO DE DADOS - UBUNTU]( )</li>
<li><b>4.3.2</b> [SCRIPT DAS TABELAS]( )</li>
</ul>

<h1><b>5.0 SPRINT 01 (10/12/2020)</h1>
<ul>
<li><b>5.1</b> [CIDADES]( ) </li>
<li><b>5.2</b> [ESTADOS]( ) </li>
</ul>        
       
<h1><b>6.0 SPRINT 02 (30/04/2020)</h1>
<ul>
<li><b>6.1</b> [MATRIZ DE HABILIDADES FINAL]( )</li>
<li><b>6.2</b> [LIÇÕES APRENDIDAS]( )</li>
<li><b>6.3</b> [DOCUMENTAÇÃO DO PROJETO NO GITLAB]( )</li>
</ul>

<h1><b>7.0 SPRINT 03 (15/05/2020)</h1>
<ul>
<li><b>7.1</b> [IMPLEMENTAÇÃO DOS RELATÓRIOS]( )</li>
<li><b>7.2</b> [IMPLEMENTAÇÃO DOS DASHBOARDS - GRÁFICOS]( )</li>
<li><b>7.3</b> [MONTAGEM DE INFRAESTRUTURA NA NUVEM ]( )</li>
<li><b>7.4</b> [DEPLOY DO PROJETO EM PRODUÇÃO]( )</li>
<li><b>7.5</b> [TESTE EM AMBIENTE DE PRODUÇÃO]( )</li>
<li><b>7.6</b> [TESTE DE CRUD DE USUÁRIO]( )</li>
</ul>

<h1><b>8.0 SPRINT 04 (28/05/2020)</h1>
<ul>
<li><b>8.1</b> [TESTE RECUPERAR SENHA POR E-MAIL]( )</li>
<li><b>8.2</b> [TESTE E VALIDAÇÃO DA FUNCIONALIDADE DO ALGORITMO]( )</li>
<li><b>8.3</b> [TESTE E VALIDAÇÃO DOS RELATÓRIOS]( )</li>
<li><b>8.4</b> [TESTE E VALIDAÇÃO DOS DASHBOARD'S - GRÁFICOS]( )</li>
<li><b>8.5</b> [ESTUDO DE USUABILIDADE (UX) - WEBSITE CONCLUÍDO]( )</li>
<li><b>8.6</b> [ESTUDO DE USUABILIDADE (UX) - RELATÓRIOS]( )</li>
<li><b>8.7</b> [ESTUDO DE USUABILIDADE (UX) - DASHBOARDS GRÁFICOS]( )</li>
</ul>

<h1><b>9.0 RELATÓRIO FINAL  (04/06/2020)</h1>
<ul>
<li><b>9.1</b> [DOCUMENTAÇÃO DO PROJETO NO GITLAB]( )</li>
<li><b>9.2</b> [MATRIZ DE HABILIDADES (FINAL)]( )</li>
<li><b>9.3</b> [LIÇÕES APRENDIDAS]( )</li>
</ul>

<h1><b>10.0 ENTREGA DO VÍDEO FINAL (30/04/2020)</h1>
<ul>
<li><b>6.1</b> [APRESENTAÇÃO DO PROJETO EM VÍDEO]( )</li>
</ul>
<br>

TIME
* [Drielli Laurine Meireles Cândido [P.O.]](https://gitlab.com/BDAg/nemasystem/wikis/Drielli)
* [Gabriela Pedroso dos Santos](https://gitlab.com/BDAg/nemasystem/wikis/Gabriela)
* [Lucas Natan Camacho da Silva](https://gitlab.com/BDAg/nemasystem/wikis/Lucas)
* [Marcos Albuquerque Macedo [S.M]](https://gitlab.com/BDAg/dadosnematologicos2019/wikis/Marcos-Albuquerque-Macedo)




Para acessar o **Cronograma** do projeto, clique [aqui](https://gitlab.com/BDAg/nemasystem/wikis/cronograma2020).


Para conhecer os **Membros da equipe**, clique [aqui](https://gitlab.com/BDAg/nemasystem/wikis/TIME).

